package com.example.claro.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMobileRepository extends CrudRepository<MobileEntity, Long>{

}
