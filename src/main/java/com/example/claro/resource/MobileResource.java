package com.example.claro.resource;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.claro.persistence.IMobileRepository;
import com.example.claro.persistence.MobileEntity;

@RestController
@RequestMapping("/claro")
@CrossOrigin("${origem-permitida}")
public class MobileResource {
	
	private static final Logger logger = LoggerFactory.getLogger(MobileResource.class);

	@Autowired
	private IMobileRepository repository;
	

	@RequestMapping(value = "/mobile", method = RequestMethod.GET)
	public ResponseEntity<List<MobileEntity>> listar() {

		List<MobileEntity> lista = (List<MobileEntity>) this.repository.findAll();

		logger.info("Retornando lista com {} celulares", lista.size());
		return new ResponseEntity<List<MobileEntity>>(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/mobile/{code}", method = RequestMethod.GET)
	public ResponseEntity<MobileEntity> buscar(@PathVariable("code") Long code) {

		Optional<MobileEntity> mobile = this.repository.findById(code);

		if (Optional.empty().equals(mobile)) {
			logger.error("Celular: {} não econtrado", mobile);
			return new ResponseEntity<MobileEntity>(HttpStatus.NOT_FOUND);
		}

		logger.info("Celular: {} retornado", mobile);
		return new ResponseEntity<MobileEntity>(mobile.get(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mobile", method = RequestMethod.POST)
    public ResponseEntity<MobileEntity> cadastrar(@RequestBody MobileEntity mobile) {
        logger.info("Cadastrando celular : {}", mobile);
        
        MobileEntity returnedMobile;
        
        try {
        	returnedMobile = this.repository.save(mobile);
        } catch(Exception e) {
        	logger.error("Não foi possível cadastrar o celular.");
        	return new ResponseEntity("Não foi possível cadastrar o celular." ,HttpStatus.CONFLICT);
        }
        logger.info("Celular: {} cadastrado com sucesso!", mobile);
 
        return new ResponseEntity<MobileEntity>(returnedMobile, HttpStatus.CREATED);
    }

}
