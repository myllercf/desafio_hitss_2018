-- Database: claro

-- DROP DATABASE claro;

CREATE DATABASE claro
  WITH OWNER = coordenador
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'pt_BR.UTF-8'
       LC_CTYPE = 'pt_BR.UTF-8'
       CONNECTION LIMIT = -1;

-------------------------------------------------------------

-- Table: mobile

-- DROP TABLE mobile;

CREATE TABLE mobile
(
  model character varying,
  price integer,
  brand character varying,
  "phot​o" character varying,
  id integer NOT NULL,
  data date,
  CONSTRAINT mobile_pkey PRIMARY KEY ()
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mobile
  OWNER TO coordenador;

-------------------------------------------------
  
 BEGIN TRANSACTION;

INSERT INTO mobile(
            id, brand, data, model, photo, price)
    VALUES (1, 'Motorola', '26/11/2015', 'Moto G5 Plus'
    , 'https://static.wmobjects.com.br/imgres/arquivos/ids/11026577-344-344/.jpg', 1990);

INSERT INTO mobile(
            id, brand, data, model, photo, price)
    VALUES (2, 'Apple', '25/12/2015', 'IPhone 7 Plus'
    , 'https://www.slickwraps.com/media/catalog/product/cache/1/image/580x580/9df78eab33525d08d6e5fb8d27136e95/i/p/iphone7plus_view1_leather_gator-white_1.jpg', 3990);

INSERT INTO mobile(
            id, brand, data, model, photo, price)
    VALUES (3, 'Samsung', '01/11/2015', 'J7'
    , 'https://static.carrefour.com.br/medias/sys_master/images/images/h5f/h36/h00/h00/8973707509790.jpg', 699);
    
COMMIT TRANSACTION;

--SELECT * FROM mobile;
--BEGIN TRANSACTION;
--delete from mobile;
--COMMIT TRANSACTION;